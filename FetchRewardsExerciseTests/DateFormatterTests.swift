//
//  DateFormatterTests.swift
//  FetchRewardsExerciseTests
//
//  Created by Anthony Benitez on 12/21/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//


import UIKit
import XCTest


class DateFormatterTests: XCTestCase {
    
    var dateFormatter: DateFormatter?
    var sampleTestDateAsString: String?
    var expectedTestDateAsString: String?
    
    override func setUp() {
        super.setUp()
        dateFormatter = DateFormatter()
        sampleTestDateAsString = "2020-12-21T16:44:31"
    }
    
    override class func tearDown() {
        super.tearDown()
    }
    
    func testConvertStringToDate() {
        let testResultDate = dateFormatter!.convertStringToDate(fromDateAsString: sampleTestDateAsString!)
        
        XCTAssertTrue(testResultDate != nil)
    }
    
    func testConvertDateToDisplay() {
        let testResultDate = dateFormatter!.convertDateToDisplay(date: dateFormatter!.convertStringToDate(fromDateAsString: sampleTestDateAsString!)!)
        let expectedResultDate = "Monday, Dec 21, 2020"

        XCTAssert(testResultDate == expectedResultDate)
    }
    
    func testConvertTimeToDisplay() {
        let testResultTime = dateFormatter!.convertTimeToDisplay(date: dateFormatter!.convertStringToDate(fromDateAsString: sampleTestDateAsString!)!)
        let expectedResultTime = "04:44 PM"
        
        XCTAssert(testResultTime == expectedResultTime)
    }
    
    func testSeparateDateAndTime() {
        let testResultDateAndTimeArray = dateFormatter!.separateDateAndTime(fromDateAsString: sampleTestDateAsString!)
        let testExpectedDateAndTimeArray = ["date": "Monday, Dec 21, 2020", "time": "04:44 PM"]
        
        XCTAssert(testResultDateAndTimeArray == testExpectedDateAndTimeArray)
    }
    
}

