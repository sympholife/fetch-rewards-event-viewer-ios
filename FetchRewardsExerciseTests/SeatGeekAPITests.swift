//
//  SeatGeekAPITests.swift
//  FetchRewardsExerciseTests
//
//  Created by Anthony Benitez on 12/21/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import UIKit
import XCTest


class SeatGeekAPITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override class func tearDown() {
        super.tearDown()
    }
    
    func testGetEventsUrlString() {
        let testResultUrl = SeatGeekAPI.shared.getEventsUrlString()
        let expectedResultUrl = "https://api.seatgeek.com/2/events?client_id=MjE0NDkzNzN8MTYwODMwMjU2NS45ODYzNDc0"
        
        XCTAssert(testResultUrl == expectedResultUrl)
    }

    func testGetEventDetailsUrlString() {
        let testResultUrl = SeatGeekAPI.shared.getEventDetailsUrlString(withEventId: "5347385")
        let expectedResultUrl = "https://api.seatgeek.com/2/events/5347385?client_id=MjE0NDkzNzN8MTYwODMwMjU2NS45ODYzNDc0"
        
        XCTAssert(testResultUrl == expectedResultUrl)
    }
    
}
