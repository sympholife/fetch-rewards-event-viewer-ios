//
//  Sizes.swift
//  FetchRewardsExercise
//
//  Created by Anthony Benitez on 12/21/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import UIKit


// MARK: - Dimensions
let kIconWidth = CGFloat(40)
let kIconHeight = CGFloat(40)
let kButtonHeight = CGFloat(50)

// MARK: - Margins
let kMarginSize_XS = CGFloat(4.0)
let kMarginSize_S = CGFloat(8.0)
let kMarginSize_M = CGFloat(16.0)
let kMarginSize_L = CGFloat(24.0)
let kMarginSize_XL = CGFloat(32.0)

// MARK: - Style Guides
let kOpacityAsCGFloat = CGFloat(0.7)
let kOpacityAsFloat = Float(0.7)
let kBorderWidth = CGFloat(2)
let kBorderRadius = CGFloat(4)
