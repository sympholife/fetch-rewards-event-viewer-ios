//
//  Fonts.swift
//  FetchRewardsExercise
//
//  Created by Anthony Benitez on 12/18/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import UIKit


// MARK: Font Sizes
let kFontSize_XS = CGFloat(12)
let kFontSize_S = CGFloat(14)
let kFontSize_M = CGFloat(16)
let kFontSize_L = CGFloat(22)
