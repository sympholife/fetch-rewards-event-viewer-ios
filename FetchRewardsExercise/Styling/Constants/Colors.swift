//
//  Colors.swift
//  FetchRewardsExercise
//
//  Created by Anthony Benitez on 12/18/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import UIKit

// MARK: - Branding Colors
let kColorFREVRed = UIColor(named: "FREVRedColor")
let kColorFREVOrange = UIColor(named: "FREVOrangeColor")

// MARK: - Layout Colors
let kColorAlwaysBlack = UIColor(named: "AlwaysBlackColor")
let kColorAlwaysWhite = UIColor(named: "AlwaysWhiteColor")
let kColorBlackLightWhiteDark = UIColor(named: "BlackLightWhiteDarkColor")
let kColorLargeTitle = UIColor(named: "LargeTitleColor")
let kColorMediumTitle = UIColor(named: "MediumTitleColor")
let kColorNavigationBar = UIColor(named: "NavigationBarColor")
let kColorSearchbarBackground = UIColor(named: "SearchbarBackgroundColor")
let kColorViewControllerBackground = UIColor(named: "ViewControllerBackgroundColor")
