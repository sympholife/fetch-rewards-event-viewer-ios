//
//  UIViewController+Extension.swift
//  FetchRewardsExercise
//
//  Created by Anthony Benitez on 12/17/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import UIKit


extension UIViewController {
    
    /*
     * Returns mock data of an array of events
     */
    func getEventsDataSource() -> [Event] {
        let debugImage = UIImage(named: IMG_DEBUG_EVENT_IMAGE)

        let events: [Event] = [
            Event(id: "1", title: "Los Angeles Rams at Tampa Bay Buccaneers", shortTitle: "Angeles Rams at Tampa", cityState: "Tampa, FL", date: "Tuesday, 24 Nov 2020", time: "01:15 AM", image: debugImage, imageURL: nil, isFavorite: false),
            Event(id: "2", title: "Atlanta Falcons at New Orleans Saints", shortTitle: "Falcons at New Orleans", cityState: "New Orleans, LA", date: "Tuesday, 24 Nov 2020", time: "06:00 PM", image: debugImage, imageURL: nil, isFavorite: false),
            Event(id: "3", title: "New Mexico Lobos at Utah State Aggies Football", shortTitle: "Lobos at Utah", cityState: "Logan, UT", date: "Thursday, 26 Nov 2020", time: "10:30 AM", image: debugImage, imageURL: nil, isFavorite: false)
        ]
            
        return events
    }
    
}

