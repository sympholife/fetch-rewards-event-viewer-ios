//
//  DateFormatter+Extension.swift
//  FetchRewardsExercise
//
//  Created by Anthony Benitez on 12/19/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import UIKit


extension DateFormatter {
    func convertStringToDate(fromDateAsString: String) -> Date? {
        let dateFormatter: DateFormatter = DateFormatter()
        let locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.locale = locale
        guard let date = dateFormatter.date(from: fromDateAsString) else {
          fatalError("Could not create date")
        }
        return date
    }
    
    func convertDateToDisplay(date: Date) -> String {
        dateFormat = "EEEE, MMM d, yyyy"
        let result = string(from: date)
        return result
    }
    
    func convertTimeToDisplay(date: Date) -> String {
        dateFormat = "hh:mm a"
        let result = string(from: date)
        return result
    }
    
    func separateDateAndTime(fromDateAsString: String) -> [String: String] {
        let dateAsDate = convertStringToDate(fromDateAsString: fromDateAsString)
        let date = convertDateToDisplay(date: dateAsDate!)
        let time = convertTimeToDisplay(date: dateAsDate!)
        
        let result = [
            "date": date,
            "time": time
        ]
        
        return result
    }
}
