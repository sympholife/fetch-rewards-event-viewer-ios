//
//  NetworkManager.swift
//  FetchRewardsExercise
//
//  Created by Anthony Benitez on 12/16/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import UIKit


class NetworkManager {

    // MARK: - Properties
    static let shared = NetworkManager()
    
    let activityIndicator = ActivitySpinnerViewController()
    
    typealias JSONDictionary = [String : Any]
    typealias JSONArray = [Any]
    
    typealias SuccessHandler = (_ json : Any) -> ()
    typealias ImageSuccessHandler = (_ image : UIImage) -> ()
    typealias ErrorHandler = (_ error : Error) -> ()
    
    lazy var defaultSession:URLSession = {
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["Content-Type":"application/json; charset=UTF-8"]
        return URLSession(configuration: config, delegate: nil, delegateQueue: nil)
        
    }()
    
    lazy var ephemeralSession:URLSession = {
        let config = URLSessionConfiguration.ephemeral
        return URLSession(configuration: config, delegate: nil, delegateQueue: nil)
    }()
    
    lazy var backgroundSession:URLSession = {
        let config = URLSessionConfiguration.background(withIdentifier: "background")
        return URLSession(configuration: config, delegate: nil, delegateQueue: nil)
    }()
    
    func getRequest(urlString: String,
                    viewController: UIViewController,
                    success: @escaping (SuccessHandler),
                    failure: @escaping (ErrorHandler)) {
        
        let url = URL(string: urlString)
        
        let urlRequest = URLRequest(url: url!)

        let task = defaultSession.dataTask(with: urlRequest, completionHandler: { (data,response,error) -> () in

            guard error == nil else {
                failure(error!)
                return
            }
            
            if let aData = data,
                let urlResponse = response as? HTTPURLResponse,
                (200..<300).contains(urlResponse.statusCode) {
                
                do {
                    let responseJSON = try JSONSerialization.jsonObject(with: aData, options: [])
                    success(responseJSON)
                }
                catch let error as NSError {
                    failure(error)
                }
            }
        })
        task.resume()
        
    }
    
    func downloadImageInMemory(urlString:String,
                               viewController: UIViewController,
                               success: @escaping (ImageSuccessHandler),
                               failure: @escaping (ErrorHandler)) {
        
        let url = URL(string: urlString)
        
        guard let unwrapped = url else {return}
        
        showSpinnerView(in: viewController)
        
        let task = defaultSession.dataTask(with: unwrapped, completionHandler: {(data,response,error) in
            
            self.hideSpinnerView(child: self.activityIndicator)
            
            guard error == nil else {
                failure(error!)
                return
            }
            
            if let aData = data,
            let urlResponse = response as? HTTPURLResponse,
                (200..<300).contains(urlResponse.statusCode) {
                
                if let image = UIImage(data: aData){
                    success(image)
                }
            }
        })
        
        task.resume()
    }
    
    func showSpinnerView(in viewController: UIViewController) {
        viewController.addChild(activityIndicator)
        activityIndicator.view.frame = CGRect(x: UIScreen.main.bounds.width - kMarginSize_M - 80, y: kMarginSize_L + kMarginSize_XS, width: kIconWidth, height: kIconHeight)
        viewController.view.addSubview(activityIndicator.view)
        activityIndicator.didMove(toParent: viewController)
    }
    
    func hideSpinnerView(child: ActivitySpinnerViewController) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            child.willMove(toParent: nil)
            child.view.removeFromSuperview()
            child.removeFromParent()
        }
    }
    
}
