//
//  Constants.swift
//  FetchRewardsExercise
//
//  Created by Anthony Benitez on 12/18/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import Foundation


class SeatGeekAPI {
    
    // MARK: - Properties
    static let shared = SeatGeekAPI()
    
    private let seatGeekUrl = "https://api.seatgeek.com/2/"
    private let eventsSub = "events"
    private let performersSub = "performers"
    private let venuesSub = "venues"
    
    // Not hiding the API Key
    private let clientId = "?client_id=MjE0NDkzNzN8MTYwODMwMjU2NS45ODYzNDc0"
    
    // MARK: - Methods
    func getEventsUrlString() -> String {
        let urlString = String(seatGeekUrl + eventsSub + clientId)
        return urlString
    }
    
    func getEventDetailsUrlString(withEventId eventId: String) -> String {
        let urlString = String(seatGeekUrl + eventsSub  + "/\(eventId)" + clientId)
        return urlString
    }
    
}
