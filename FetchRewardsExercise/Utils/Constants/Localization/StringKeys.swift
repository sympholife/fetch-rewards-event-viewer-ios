//
//  StringKeys.swift
//  FetchRewardsExercise
//
//  Created by Anthony Benitez on 12/19/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import Foundation


/* EventsController */
let TXT_EVENTS_TITLE = NSLocalizedString("TXT_EVENTS_TITLE", comment: "Title in EventsController")
let TXT_EVENTS_LOADING = NSLocalizedString("TXT_EVENTS_LOADING", comment: "Text for loading spinner in EventsController")
let TXT_EVENTS_SEARCH_PLACEHOLDER = NSLocalizedString("TXT_EVENTS_SEARCH_PLACEHOLDER", comment: "Searchbar placeholder in EventsController")
let TXT_EVENTS_FAVORITE = NSLocalizedString("TXT_EVENTS_FAVORITE", comment: "Title in EventsController")

/* EventDetailsController */
let TXT_EVENTS_DETAILS_BACK = NSLocalizedString("TXT_EVENTS_DETAILS_BACK", comment: "Back button text in EventDetailsController")
let TXT_EVENTS_ADD_FAVORITE = NSLocalizedString("TXT_EVENTS_ADD_FAVORITE", comment: "Favorite button to add in EventDetailsController")
let TXT_EVENTS_REMOVE_FAVORITE = NSLocalizedString("TXT_EVENTS_REMOVE_FAVORITE", comment: "Favorite button to remove in EventDetailsController")
