//
//  EventDetailsView.swift
//  FetchRewardsExercise
//
//  Created by Anthony Benitez on 12/17/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import UIKit

class EventDetailsView: UIView {
    
    // MARK: - Properties
    public var event: Event?
    
    private let containerView:UIView = {
        let view = UIView()
        return view
    }()
    
    private let eventImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.configureShadowEffect()
        return imageView
    }()
    
    private let eventDateTimeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: kFontSize_L)
        label.textColor = kColorLargeTitle
        label.numberOfLines = 0
        return label
    }()
    
    private let eventCityStateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: kFontSize_M)
        label.textColor = kColorMediumTitle
        return label
    }()
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(containerView)
        containerView.anchor(top: topAnchor,
                             left: leftAnchor,
                             right: rightAnchor)
        
        containerView.addSubview(eventImageView)
        eventImageView.setDimensions(height: UIScreen.main.bounds.width - kMarginSize_XL, width: UIScreen.main.bounds.width)
        eventImageView.anchor(top: containerView.topAnchor,
                              left: containerView.leftAnchor,
                              right: containerView.rightAnchor,
                              paddingLeft: kMarginSize_M,
                              paddingRight: kMarginSize_M)
        
        containerView.addSubview(eventDateTimeLabel)
        eventDateTimeLabel.anchor(top: eventImageView.bottomAnchor,
                                   left: eventImageView.leftAnchor,
                                   right: eventImageView.rightAnchor,
                                   paddingTop: kMarginSize_M)
        
        containerView.addSubview(eventCityStateLabel)
        eventCityStateLabel.anchor(top: eventDateTimeLabel.bottomAnchor,
                                   left: eventDateTimeLabel.leftAnchor,
                                   bottom: containerView.bottomAnchor,
                                   paddingTop: kMarginSize_XS,
                                   paddingBottom: kMarginSize_S)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width - 32 + 22 + 14 + 4 + 8 + 16)
    }
    
    // MARK: - Helpers
    func createEventDetailsViewData(withEvent event: Event) {
        eventImageView.image = event.image
        eventCityStateLabel.text = event.cityState
        eventDateTimeLabel.text = event.date! + ", " + event.time!
    }

}
