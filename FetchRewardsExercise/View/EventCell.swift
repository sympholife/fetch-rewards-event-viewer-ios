//
//  EventCell.swift
//  FetchRewardsExercise
//
//  Created by Anthony Benitez on 12/17/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import UIKit


class EventCell: UITableViewCell {
    
    // MARK: - Properties
    var representedIdentifier: String = ""
    
    private let containerView: UIImageView = {
        let view = UIImageView()
        return view
    }()
    
    private let eventImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.borderColor = kColorBlackLightWhiteDark?.cgColor
        imageView.layer.borderWidth = kBorderWidth
        imageView.layer.opacity = kOpacityAsFloat
        return imageView
    }()
    
    private let eventTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: kFontSize_L)
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private let eventCityStateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: kFontSize_M)
        label.textColor = kColorAlwaysWhite
        return label
    }()
    
    private let eventDateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: kFontSize_M)
        label.textColor = kColorAlwaysWhite
        return label
    }()
    
    private let eventTimeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: kFontSize_M)
        label.textColor = kColorAlwaysWhite
        return label
    }()
    
    private let eventFavoriteLabel: UILabel = {
        let label = UILabel()
        let font = UIFont(name: "Swish Buttons NF", size: kFontSize_L)
        label.font = font
        label.textColor = .white
        label.text = TXT_EVENTS_FAVORITE
        return label
    }()
    
    // MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = kColorViewControllerBackground
        self.selectionStyle = .none;

        addSubview(containerView)
        containerView.anchor(top: topAnchor,
                             left: leftAnchor,
                             bottom: bottomAnchor,
                             right: rightAnchor,
                             paddingTop: kMarginSize_S,
                             paddingLeft: kMarginSize_S,
                             paddingBottom: kMarginSize_S,
                             paddingRight: kMarginSize_S)
        containerView.setNeedsLayout()
        containerView.layoutIfNeeded()
        containerView.configureShadowEffect()
        
        containerView.addSubview(eventImageView)
        eventImageView.anchor(top: containerView.topAnchor,
                              left: containerView.leftAnchor,
                              bottom: containerView.bottomAnchor,
                              right: containerView.rightAnchor)

        containerView.addSubview(eventTitleLabel)
        eventTitleLabel.anchor(top: containerView.topAnchor,
                               left: containerView.leftAnchor,
                               right: containerView.rightAnchor,
                               paddingTop: kMarginSize_M,
                               paddingLeft: kMarginSize_M,
                               paddingRight: kMarginSize_M)

        containerView.addSubview(eventCityStateLabel)
        eventCityStateLabel.anchor(top: eventTitleLabel.bottomAnchor,
                                   left: eventTitleLabel.leftAnchor,
                                   paddingTop: kMarginSize_M)

        containerView.addSubview(eventDateLabel)
        eventDateLabel.anchor(top: eventCityStateLabel.bottomAnchor,
                              left: eventCityStateLabel.leftAnchor,
                              paddingTop: kMarginSize_XS)
        
        containerView.addSubview(eventTimeLabel)
        eventTimeLabel.anchor(top: eventDateLabel.bottomAnchor,
                              left: eventDateLabel.leftAnchor,
                              paddingTop: kMarginSize_XS)
        
        containerView.addSubview(eventFavoriteLabel)
        eventFavoriteLabel.anchor(bottom: eventTimeLabel.bottomAnchor,
                                  right: eventTitleLabel.rightAnchor)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Helpers
    func createEventCellData(withEvent event: Event) {
        eventTitleLabel.text = event.title
        eventCityStateLabel.text = event.cityState
        eventDateLabel.text = event.date
        eventTimeLabel.text = event.time
        eventImageView.image = event.image
        eventFavoriteLabel.isHidden = !event.isFavorite!
    }

}
