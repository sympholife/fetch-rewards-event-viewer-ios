//
//  EventDetailsController.swift
//  FetchRewardsExercise
//
//  Created by Anthony Benitez on 12/17/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import UIKit


protocol EventsDetailControllerDelegate: AnyObject {
    func eventFavoriteChanged(isFavorited: Bool, event: Event)
}

class EventDetailsController: UIViewController {
    
    // MARK: - Properties
    public var event: Event!
    private var eventsDetailsView: EventDetailsView!
    weak var delegate: EventsDetailControllerDelegate?
    
    private let favoriteHeartImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: IMG_FAVORITE_HEART_IMAGE)
        imageView.isHidden = true
        return imageView
    }()
    
    private let favoriteButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
        button.layer.cornerRadius = kBorderRadius
        button.setHeight(kButtonHeight)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: kFontSize_L)
        button.addTarget(self, action: #selector(handleFavoriteEvent), for: .touchUpInside)
        return button
    }()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureNavigationController()
    }
    
    //MARK: - Actions
    @objc func handleFavoriteEvent() {
        let isNowFavorited = !(event?.isFavorite!)!
        
        updateFavoriteButton(isFavorited: isNowFavorited)
        showOrHideFavoriteHeart(isFavorited: isNowFavorited)
        delegate?.eventFavoriteChanged(isFavorited: isNowFavorited, event: event!)
    }
    
    // MARK: - Helpers
    private func configureUI() {
        view.backgroundColor = kColorNavigationBar

        eventsDetailsView = EventDetailsView()
        eventsDetailsView.createEventDetailsViewData(withEvent: event!)
        view.addSubview(eventsDetailsView)
        eventsDetailsView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                                 left: view.leftAnchor,
                                 right: view.rightAnchor,
                                 paddingTop: kMarginSize_M)
        
        view.addSubview(favoriteHeartImageView)
        favoriteHeartImageView.setDimensions(height: kIconHeight, width: kIconWidth)
        favoriteHeartImageView.anchor(top: eventsDetailsView.topAnchor,
                                      left: eventsDetailsView.leftAnchor,
                                      paddingTop: kMarginSize_XL + kMarginSize_L,
                                      paddingLeft: kMarginSize_L)
        showOrHideFavoriteHeart(isFavorited: event.isFavorite!)
        
        view.addSubview(favoriteButton)
        favoriteButton.anchor(left: view.leftAnchor,
                              bottom: view.safeAreaLayoutGuide.bottomAnchor,
                              right: view.rightAnchor,
                              paddingLeft: kMarginSize_M,
                              paddingBottom: kMarginSize_M,
                              paddingRight: kMarginSize_M)
        
        updateFavoriteButton(isFavorited: event!.isFavorite!)
    }
    
    private func configureNavigationController() {
        navigationItem.title = event!.shortTitle
        for navItem in (self.navigationController?.navigationBar.subviews)! {
            for itemSubView in navItem.subviews {
                if let largeLabel = itemSubView as? UILabel {
                    largeLabel.text = self.title
                    largeLabel.numberOfLines = 0
                    largeLabel.lineBreakMode = .byWordWrapping
                }
            }
        }
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: kFontSize_L)]
        navigationController?.navigationBar.tintColor = kColorLargeTitle
        navigationController?.navigationBar.backgroundColor = kColorNavigationBar
        navigationController?.view.backgroundColor = kColorNavigationBar
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationItem.largeTitleDisplayMode = .always
    }
    
    private func showOrHideFavoriteHeart(isFavorited: Bool) {
        if (isFavorited) {
            UIView.animate(withDuration: 2.0, animations: {
                self.favoriteHeartImageView.isHidden = false
            })
            
            let pulse = CASpringAnimation(keyPath: "transform.scale")
            pulse.duration = 0.4
            pulse.fromValue = 1.0
            pulse.toValue = 1.12
            pulse.autoreverses = true
            pulse.repeatCount = .infinity
            pulse.initialVelocity = 0.5
            pulse.damping = 0.8
            favoriteHeartImageView.layer.add(pulse, forKey: nil)
        } else {
            UIView.animate(withDuration: 2.0, animations: {
                self.favoriteHeartImageView.isHidden = true
            })
        }
    }
    
    private func updateFavoriteButton(isFavorited: Bool) {
        if (isFavorited) {
            favoriteButton.setTitle(TXT_EVENTS_REMOVE_FAVORITE, for: .normal)
            favoriteButton.backgroundColor = kColorFREVOrange
        } else {
            favoriteButton.setTitle(TXT_EVENTS_ADD_FAVORITE, for: .normal)
            favoriteButton.backgroundColor = kColorFREVRed
        }
    }

}
