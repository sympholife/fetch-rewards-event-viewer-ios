//
//  EventsController.swift
//  FetchRewardsExercise
//
//  Created by Anthony Benitez on 12/16/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import UIKit


private let eventCellReuseIdentifier = "eventCellRI"

class EventsController: UITableViewController {
    
    // MARK: - Properties
    public var events: [Event] = []
    private var filteredEvents: [Event] = []
    var searchController = UISearchController(searchResultsController: nil)
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    var isFiltering: Bool {
        return searchController.isActive && !isSearchBarEmpty
    }
    
    var dateFormatter: DateFormatter = DateFormatter()

    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        configureNavigationController()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSearchController()
        configureUI()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        searchController.isActive = false;
    }
    
    // MARK: - Helpers
    func configureUI() {
        self.tableView.backgroundColor = kColorViewControllerBackground
        tableView.register(EventCell.self, forCellReuseIdentifier: eventCellReuseIdentifier)
        tableView.separatorStyle = .none
                getEventsDataFromService()
    }
    
    func configureNavigationController() {
        navigationItem.title = TXT_EVENTS_TITLE
        navigationController?.navigationBar.barTintColor = kColorNavigationBar
        let textAttributes = [NSAttributedString.Key.foregroundColor:kColorLargeTitle, NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: kFontSize_L)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes as [NSAttributedString.Key : Any]
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationItem.largeTitleDisplayMode = .never
    }
    
    func configureSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        if #available(iOS 13.0, *) {
            searchController.searchBar.searchTextField.backgroundColor = kColorSearchbarBackground
        } else {
            for view : UIView in (searchController.searchBar.subviews[0]).subviews {
                if let textField = view as? UITextField {
                    textField.backgroundColor = kColorSearchbarBackground
                }
            }
        }
        searchController.searchBar.tintColor = kColorAlwaysBlack
        UIBarButtonItem.appearance(whenContainedInInstancesOf:[UISearchBar.self]).tintColor = kColorLargeTitle

        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = TXT_EVENTS_SEARCH_PLACEHOLDER

        searchController.searchBar.autocapitalizationType = .none
        searchController.searchBar.delegate = self 
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredEvents = events.filter { (event: Event) -> Bool in
            return event.title!.lowercased().contains(searchText.lowercased())
        }
        tableView.reloadData()
    }
    
    

}

// MARK: Events API Request
extension EventsController {
    
    func getEventsDataFromService() {
        
        let eventsUrl = SeatGeekAPI.shared.getEventsUrlString()
        
        NetworkManager.shared.getRequest(urlString: eventsUrl, viewController: self.navigationController!, success: { (result) in
    
            var createdEvent = Event()
        
            let data: [String: Any] = result as! [String : Any]
            if let fetchedEvents: [[String: Any]] = data["events"] as? [[String: Any]] {
                for event in fetchedEvents {
                    if let venue: [String: Any] = event["venue"] as? [String: Any], let performers: [[String: Any]] = event["performers"] as? [[String: Any]] {
                        
                        let id: NSNumber = event["id"] as? NSNumber ?? 0
                        let title: String = event["title"] as? String ?? "???"
                        let shortTitle: String = event["short_title"] as? String ?? "???"
                        let cityState: String = venue["display_location"] as? String ?? "???"
                        let dateTime = event["datetime_local"] as? String ?? "9999-01-01T09:09:09"
                        let image: UIImage? = nil
                        var imageURL: String = ""
                        let isFavorite = UserDefaults.standard.bool(forKey: id.stringValue)
                        
                        let explodedDateAndTime = self.dateFormatter.separateDateAndTime(fromDateAsString: dateTime)
                        let date = explodedDateAndTime["date"]
                        let time = explodedDateAndTime["time"]
                        
                        let firstPerformer = performers[0]
                        if let imageAttribution: String = firstPerformer["image"] as? String {
                            imageURL = imageAttribution
                        }
                        
                        createdEvent = Event(id: id.stringValue,
                                             title: title,
                                             shortTitle: shortTitle,
                                             cityState: cityState,
                                             date: date,
                                             time: time,
                                             image: image,
                                             imageURL: imageURL,
                                             isFavorite: isFavorite)
                        self.events.append(createdEvent)
                        
                        
                        self.events = self.events.sorted { $0.id! < $1.id! }
                        
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                        
                    }
                
                }
            }
            
        }) { (error) in
            print("ERROR: \(error.localizedDescription)")
        }
        
    }
    
}

// MARK: - EventDetailsControllerDelegate protocols
extension EventsController: EventsDetailControllerDelegate {
    func eventFavoriteChanged(isFavorited: Bool, event: Event) {
        event.isFavorite = isFavorited
        UserDefaults.standard.set(isFavorited, forKey: event.id!)
        tableView.reloadData()
    }
}

// MARK: - UITableViewDataSource protocols
extension EventsController {
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 208
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
          return filteredEvents.count
        }
        return events.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: eventCellReuseIdentifier, for: indexPath) as! EventCell
        var event = events[indexPath.row]
        
        if isFiltering {
          event = filteredEvents[indexPath.row]
        }
        
        let imageURL = event.imageURL
        
        if imageURL != nil || imageURL != "" {
            DispatchQueue.main.async {
                NetworkManager.shared.downloadImageInMemory(urlString: imageURL!, viewController: self.navigationController!, success: { (downloadedImage) in
                DispatchQueue.main.async {
                    event.image = downloadedImage
                    cell.createEventCellData(withEvent: event)
                }}) { (error) in
                    print("Error downloading image: \(error.localizedDescription)")
                    event.image = UIImage(named: "debug_event_image")
                    cell.createEventCellData(withEvent: event)
                }
            }
        }

        return cell
    }
}

// MARK: - UITableViewDelegate protocols
extension EventsController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedEvent = events[indexPath.row]
        if isFiltering {
            selectedEvent = filteredEvents[indexPath.row]
        }
        let eventDetailsController = EventDetailsController()
        eventDetailsController.delegate = self
        eventDetailsController.event = selectedEvent
        navigationItem.backBarButtonItem = UIBarButtonItem(title: TXT_EVENTS_DETAILS_BACK, style: .plain, target: nil, action: nil)
        navigationController?.pushViewController(eventDetailsController, animated: true)
    }
}

// MARK: - UISearchResultsUpdating
extension EventsController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}


// MARK: - UISearchBarDelegate protocols
extension EventsController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchController.isActive = false;
    }

    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        updateSearchResults(for: searchController)
    }

}
