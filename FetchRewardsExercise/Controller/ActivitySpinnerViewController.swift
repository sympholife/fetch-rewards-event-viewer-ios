//
//  ActivitySpinnerViewController.swift
//  FetchRewardsExercise
//
//  Created by Anthony Benitez on 12/21/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import UIKit


class ActivitySpinnerViewController: UIViewController {
    
    // MARK: - Properties
    var activityIndicatorView = UIActivityIndicatorView()

    // MARK: - Methods
    override func loadView() {
        if #available(iOS 13.0, *) {
            activityIndicatorView = UIActivityIndicatorView(style: .medium)
        } else {
            activityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)
        }
        self.view = UIView()
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.anchor(top: self.view.topAnchor,
                                     left: self.view.rightAnchor,
                                     width: kIconWidth,
                                     height: kIconHeight)
        activityIndicatorView.startAnimating()
    }
    
}


