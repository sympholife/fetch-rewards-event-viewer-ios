//
//  Event.swift
//  FetchRewardsExercise
//
//  Created by Anthony Benitez on 12/17/20.
//  Copyright © 2020 Sympholife. All rights reserved.
//

import UIKit


class Event {
    // MARK: - Properties
    let id: String?
    let title: String?
    let shortTitle: String?
    let cityState: String?
    let date: String?
    let time: String?
    var image: UIImage?
    var imageURL: String?
    var isFavorite: Bool?

    // MARK: - Lifecycle
    init() {
        self.id = nil
        self.title = nil
        self.shortTitle = nil
        self.cityState = nil
        self.date = nil
        self.time = nil
        self.image = nil
        self.imageURL = nil
        self.isFavorite = false
    }
    
    init(id: String, title: String, shortTitle: String, cityState: String?, date: String?, time: String?, image: UIImage?, imageURL: String?, isFavorite: Bool?) {
        self.id = id
        self.title = title
        self.shortTitle = shortTitle
        self.cityState = cityState
        self.date = date
        self.time = time
        self.image = image
        self.imageURL = imageURL
        self.isFavorite = isFavorite
    }
}
